#include <msp430.h> 
#include <stdint.h>

/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weer te geven
 * temperatuur aan...
 *
 * int temp: De weer te geven temperatuur.
 * Max is 999, min is -99.
 * Hierbuiten wordt "Err-" weergegeven.
 *
 * Voorbeeld:
 * setTemp(100); // geef 10.0 graden weer
 */
void setTemp(int temp);

/* Deze functie geeft bovenaan in het
 * display een titel weer.
 * char tekst[]: de weer te geven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); // geef Hallo weer
 */
void setTitle(char tekst[]);

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer
	
	// Stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;

	initDisplay();
	setTitle("Opdracht7.1.13");

	uint8_t temperatuur = 0; // Fictieve temperatuur

	while(1)
	{
	    // Hoog fictieve temperatuur op met 0.1 graad
	    // Geef dit weer op het display
	    setTemp(temperatuur++);
	    // Niet te snel...
	    __delay_cycles(4000000);
	}
}
